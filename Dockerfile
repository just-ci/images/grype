FROM docker.io/alpine:3

RUN apk add --no-cache skopeo curl

RUN curl -sSfL https://raw.githubusercontent.com/anchore/grype/main/install.sh | sh -s -- -b /usr/local/bin
