# 1.0.0 (2021-11-01)


### Bug Fixes

* fixes docker image ([396f814](https://gitlab.com/just-ci/images/grype/commit/396f814e7b9555b29f99a140a2e484a90fb569ee))
